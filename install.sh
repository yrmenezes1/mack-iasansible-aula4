echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Instalacao de pacotes"
apt install software-properties-common curl gnupg2 wget python3-pip git pwgen unzip -y

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Repositorio Docker"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt update -y

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Instalacao Docker"

apt install docker-ce docker-ce-cli containerd.io -y
systemctl start docker
systemctl enable docker

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Instalacao Docker-compose"
apt install docker-compose -y
pip3 install docker-compose==1.25.0

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Instalacao Ansible"
apt install ansible -y

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Instalacao NPM"
apt install nodejs npm -y
npm install npm --global

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Clone AWX"
wget https://github.com/ansible/awx/archive/17.1.0.zip
unzip 17.1.0.zip
cd awx-17.1.0/installer

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Geracao de Chave"
pwgen -N 1 -s 30

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Alterando o arquivo"
#admin_user=admin
sed -i 's/# admin_password=passwor/admin_password=passwor/g' inventory
#admin_password=password
#secret_key=gBSBmZgb1CyuT6p6TZ4D0pF2oZMP4x

exit 0

echo "`date +%y"/"%m"/"%d" "%H":"%M":"%S` Instalacao Ansible AWX"
ansible-playbook -i inventory install.yml
